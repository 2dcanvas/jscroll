
// Content container element in out HTML
container = document.getElementById("container")

// If found, Initialize values for the container
if (container)
{
  container.x = parseFloat(container.style.left);
  container.y = parseFloat(container.style.top);
  container.updateposition = function() {

    container.style.left = container.x + 'px'; container.style.top = container.y + 'px';
  };
  // previousLocation stores the last location
  previous = {x: container.x, y: container.y};
  zoom = 1;   // zoom = parseFloat(container.getAttribute('data-zoom'));
}


// Initialize dragging variable, a bool that decides whether we're dragging
// the screen
dragging = null;

window.onmousedown = function(e) {
  movingtext = null;
  dragging = true;

  biggestpictureseen = false;
  previous = {x: e.pageX, y: e.pageY};

}

window.onmouseup = function() {
  dragging = false;
}

window.ondragstart = function(e) {
  e.preventDefault();
}

window.onmousemove = function(e) {
  if (dragging) {
    // console.log(container.x + ' ' + container.y);
    container.style.transitionDuration = "0s";
    container.x += e.pageX - previous.x;
    container.y += e.pageY - previous.y;
    container.updateposition();
    previous = {x: e.pageX,y: e.pageY};
  }
}
