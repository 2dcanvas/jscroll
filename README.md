JScroll
=
Small Javascript library that allows panning relative to a containing div.

Installation
-
This library requires jQuery ro run, to install from cdn, paste the following into your html:

`<script src="http://code.jquery.com/jquery-1.12.0.min.js"></script>`

Make sure to include jQuery before including the jscroll library

Usage
-
After including the library make sure you have a div in your html with id `container`, div inside this container must set their position relative to their parent container div.

The page can be scrolled by holding the left mouse button and dragging.


